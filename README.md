PIT - Exercise Prolog Interpreter
=================================

This project selects a subset of the Prolog language and implements SLD and coSLD.

This is a course project.  It is **not** intended to be a complete Prolog interpreter.
That said, if you are still interested in extending this project, feel free to do so.
I would be open to pull requests as I'm done with the course.
