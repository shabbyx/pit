module Main where

import Prelude hiding (lex)
import Text.Pit
import Control.Monad.Writer
import Data.Maybe
import Data.List (intercalate)
import System.Console.GetOpt
import System.Environment
import System.Exit

data Opt = Opt
    { optCoSld :: Bool
    , optHelp :: Bool
    }

defaultOpts :: Opt
defaultOpts = Opt
    { optCoSld = False
    , optHelp = False
    }

options :: [OptDescr (Opt -> Opt)]
options =
    [ Option ['h'] ["help"] (NoArg (\opts -> opts { optHelp = True})) "Shows this help"
    , Option ['c'] ["cosld"] (NoArg (\opts -> opts { optCoSld = True})) "Solve queries with coSLD"
    ]

usageMessage :: String -> [OptDescr (Opt -> Opt)] -> String
usageMessage prog = usageInfo ("Usage: " ++ prog ++ " [OPTIONS]")

compileArgs :: String -> [String] -> IO Opt
compileArgs prog argv = case getOpt Permute options argv of
                    (o, _, []) -> return (foldl (flip id) defaultOpts o)
                    (_, _, errs) -> ioError (userError (concat errs ++ usageInfo ("Usage: " ++ prog ++ " [OPTIONS]") options))

-- There will be three sources of messages: Lexer errors, Parser errors and query results
-- The mergeMessages function takes Message lists and returns a merged list of Strings based on their line numbers.
merge2 :: [Message] -> [Message] -> [Message]
merge2 [] ys = ys
merge2 xs [] = xs
merge2 (x@(_, lx, cx):xs) (y@(_, ly, cy):ys)
    | lx < ly || lx == ly && cx < cy = x:merge2 xs (y:ys)
    | otherwise                      = y:merge2 (x:xs) ys
mergeMessages :: [[Message]] -> [String]
mergeMessages = map showMessage . foldr merge2 []

compile :: Opt -> String -> [String]
compile opts content = mergeMessages [lm, pm, map (\(q, l, c) -> (showStatement (Query q) ++ "\n" ++ solve q, l, c)) queries]
            where
                (tokens, lm) = runWriter $ lex content
                (statements, pm) = runWriter $ parse tokens

                fromQuery :: (Statement, Int, Int) -> Maybe (Definition, Int, Int)
                fromQuery ((Query q), l, c) = Just (q, l, c)
                fromQuery _ = Nothing

                queries = mapMaybe fromQuery statements

                solve :: Definition -> String
                solve q = if null subs then "false.\n"
                          else (++ if more then ";\n\n...\n" else ".\n") . intercalate ";\n\n" . map (showSubstitution . expandSubstitution) $ showable
                    where
                        subs = solveQuery (map (\(s, _, _) -> s) statements) q (optCoSld opts)
                        showable = take 10 subs
                        more = length (take 11 subs) > 10

main :: IO ()
main = do
            prog <- getProgName
            args <- getArgs
            opts <- compileArgs prog args
            when (optHelp opts) $ do
                putStrLn (usageMessage prog options)
                exitWith ExitSuccess
            getContents >>= putStrLn . unlines . compile opts
