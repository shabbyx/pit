module Text.Pit.Sld (
        solveQuery,
        ) where

import qualified Data.Map as Map
import Control.Monad.State
import Data.Maybe
import Text.Pit.Types
import Text.Pit.Unification

-- `solveQuery` takes a Query and returns a (possibly infinite) list of substitutions that satisfy the query
-- `And` definitions restrict the substitution further while `Or` definitions create more possibilites.
-- `Unification` is done through `unify`.  `Requisite` results in substitutions either through SLD or coSLD.
-- The second argument of solveQuery is thus either False for SLD or True for coSLD.
solveQuery :: [Statement] -> Definition -> Bool -> [Substitution]
solveQuery statements definition cosld = map fst . evalState (solve (emptySubstitution, []) definition) $ 0
    where
        solve :: (Substitution, [Term]) -> Definition -> State Int [(Substitution, [Term])]
        -- if looking at an atom already present in history, then take it from the history
        solve (subst, history) (Requisite t@(Structure _ _))
            | cosld && not (null historyMatch) = return . map (\s -> (s, history)) $ historyMatch
                where
                    historyMatch = catMaybes . map (\h -> unifyAdd t h subst) $ history
        -- if it's a fact, then it holds and there is nothing to substitute
        solve (subst, history) Fact = return [(subst, history)]
        -- if it's a unification, both sides need to be unified, and no expansion is necessary
        solve (subst, history) (Unification t1 t2) = return . map (\s -> (s, history)) . maybeToList $ unifyAdd t1 t2 subst
        -- if it's an And, both sides need to be satisfied and their substitutions merged.
        -- indeed, the substitution from the left-hand side needs to be present when solving the
        -- right-hand side.  For example, `Y = add(s(z), z, s(z)), Y` should verify that Y holds.
        solve (subst, history) (And d1 d2) = do
                                s1 <- solve (subst, history) d1
                                s2s <- mapM (\s -> solve s d2) s1
                                return . concat $ s2s
        -- if it's an Or, at least one side needs to be satisfied
        solve (subst, history) (Or d1 d2) = do
                                s1 <- solve (subst, history) d1
                                s2 <- solve (subst, history) d2
                                return $ s1 ++ s2
        -- if it's just a variable and it is already in subst and it expands to an atom, that atoms needs to be satisfied.
        -- Otherwise, it's an error (swipl in this case says: 'Arguments are not sufficiently instantiated')
        solve (subst, history) (Requisite (Assignable _ x)) = case lookupSubstitution x subst of
                                                        Just (_, Just atom) -> solve (subst, history) $ Requisite atom
                                                        _ -> return []
        -- if it's an atom, then the statements need to be searched for a Declaration with a matching head.  For each matching head atom,
        -- the statement is recreated with fresh variables, during which the variables replaced so far are kept so if the same variable was
        -- revisited it would be replaced with the same name.  Then the atom is properly unified with the head of the statement.  If that
        -- is successful, then the resulting substitution is merged with subst and solve is called again with the body of the statement.
        -- This is SLD! TODO: add history somewhere to accomodate for coSLD!
        solve (subst, history) (Requisite t@(Structure atom args)) = do
                                                            candidates <- mapM replaceVars . filter matchesAtom $ statements
                                                            substs <- mapM solveRecusive candidates
                                                            return $ concat substs
            where
                matchesAtom :: Statement -> Bool
                matchesAtom (Declaration (Structure a g) _) = atom == a && length args == length g
                matchesAtom _ = False

                replaceVars :: Statement -> State Int Statement
                replaceVars s = do
                                n <- get
                                let (sres, (_, nres)) = runState (replaceVarsStatement s) (Map.empty, n)
                                put nres
                                return sres
                    where
                        replaceVarsStatement :: Statement -> State (Map.Map String String, Int) Statement
                        replaceVarsStatement (Declaration st b) = do
                                                                    sres <- replaceVarsTerm st
                                                                    bres <- replaceVarsDefinition b
                                                                    return $ Declaration sres bres
                        replaceVarsStatement _ = error "internal error: replaceVarsStatement shouldn't have been called with a Query"

                        replaceVarsTerm :: Term -> State (Map.Map String String, Int) Term
                        replaceVarsTerm (Structure a g) = do
                                                            gres <- mapM replaceVarsTerm g
                                                            return $ Structure a gres
                        replaceVarsTerm (Assignable v _) = do
                                                            -- when replacing a variable, check first if it's already replaced
                                                            (m, n) <- get
                                                            let (r, mres, nres) = case Map.lookup v m of
                                                                                    Just old -> (old, m, n)
                                                                                    Nothing -> (new, Map.insert v new m, n + 1)
                                                                                        where
                                                                                            new = "$" ++ show n
                                                            put (mres, nres)
                                                            return $ Assignable v r

                        replaceVarsDefinition :: Definition -> State (Map.Map String String, Int) Definition
                        replaceVarsDefinition Fact = return $ Fact
                        replaceVarsDefinition (Requisite t1) = do
                                                                tres1 <- replaceVarsTerm t1
                                                                return $ Requisite tres1
                        replaceVarsDefinition (Unification t1 t2) = do
                                                                tres1 <- replaceVarsTerm t1
                                                                tres2 <- replaceVarsTerm t2
                                                                return $ Unification tres1 tres2
                        replaceVarsDefinition (Or d1 d2) = do
                                                                dres1 <- replaceVarsDefinition d1
                                                                dres2 <- replaceVarsDefinition d2
                                                                return $ Or dres1 dres2
                        replaceVarsDefinition (And d1 d2) = do
                                                                dres1 <- replaceVarsDefinition d1
                                                                dres2 <- replaceVarsDefinition d2
                                                                return $ And dres1 dres2

                solveRecusive :: Statement -> State Int [(Substitution, [Term])]
                solveRecusive (Declaration h b) = case unifyAdd t h subst of
                                                    Just headsubst -> solve (headsubst, newHistory) b
                                                        where newHistory = if cosld then h:history else history
                                                    Nothing -> return []
                solveRecusive (Query _) = error "internal error: solveRecusive shouldn't have been called with a Query"
