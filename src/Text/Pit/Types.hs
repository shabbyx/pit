module Text.Pit.Types (
        -- general compiler data
        Message,
        showMessage,
        -- Lexer
        TokenType(..),
        Token,
        getTokenType,
        getTokenPosition,
        -- Parser
        Term(..),
        Definition(..),
        Statement(..),
        showTerm,
        showDefinition,
        showStatement,
        -- Unification
        Substitution,
        emptySubstitution,
        showSubstitution,
        lookupSubstitution,
        ) where

import Data.List (intercalate, isPrefixOf)
import Data.Maybe (isNothing, listToMaybe, catMaybes)
import Prelude hiding (showList)
import qualified Data.Map as Map -- TODO: use Strict version as per [recommendation](http://hackage.haskell.org/package/containers-0.5.5.1/docs/Data-Map-Lazy.html)?
import qualified Data.Set as Set

-- A message is a string accompanied by a line and column where it got originated
type Message = (String, Int, Int)

showMessage :: Message -> String
showMessage (s, l, c) = show l ++ ":" ++ show c ++ ": " ++ s

-- The token types understood by the lexer
data TokenType = TParenOpen         -- (
                | TParenClose       -- )
                | TComma            -- ,
                | TDot              -- .
                | TImpliedBy        -- :-
                | TQuery            -- ?-
                | TEqual            -- =
                | TBracketOpen      -- [
                | TBracketClose     -- ]
                | TBar              -- |
                | TOr               -- ;
                | TWhiteSpace       -- anything accepted by isSpace and comments
                | TVariable String  -- A variable name matching [A-Z][A-Za-z0-9_]* | _[A-Za-z0-9_]\+
                | TWildCard         -- _
                | TAtom String      -- An atom matching '\([^']\|''\)*' | [a-z][A-Za-z0-9_]* | graphic\+
                                    -- where graphic characters are one of #&*+-./:<=>?@\^~
                                    -- See: http://www.amzi.com/manuals/amzi/pro/ref_terms.htm
                | TEnd              -- End of the stream
               deriving (Show, Eq)

-- A token is a token type along with information such as line number and column number
type Token = (TokenType, Int, Int)

getTokenType :: Token -> TokenType
getTokenType (t, _, _) = t

getTokenPosition :: Token -> String
getTokenPosition (_, l, c) = show l ++ ":" ++ show c

-- A term is a structure, variable, wildcard or a list.
data Term = Structure String [Term]                 -- A structure is an atom with a set of terms as arguments
                                                    -- Note: A list is a structure with name "[" and contains a head and a tail
           | Assignable String String               -- An assignable is either a variable or a wildcard ("_")
                                                    -- Note: The second string is used during (co)SLD to give new names to variables for unification
                    deriving (Show, Eq)
-- A definition is a combination of tokens that define an atom and consist of requisites (term or unification) that are "and"ed or "or"ed.
data Definition = Requisite Term | Unification Term Term | Or Definition Definition | And Definition Definition | Fact
                    deriving (Show, Eq)
-- A statement is either a declaration or a query, which is one processing unit
data Statement = Declaration Term Definition        -- A declaration is a definition for a structure.  Term is used here for head,
                                                    -- but other types of terms are invalid
                | Query Definition                  -- A query has the same form of a definition
                    deriving (Show, Eq)

showList :: [Term] -> String
showList [] = ""
showList [x, Structure "[" []] = showTerm x
showList [x, Structure "[" xs] = showTerm x ++ ", " ++ showList xs
showList [x, y] = showTerm x ++ "|" ++ showTerm y
showList ts = error $ "internal error: bad list format (list arguments are always 0 or 2, but\n"
                ++ "  this list has " ++ show (length ts) ++ " arguments.\n"
                ++ "  The list arguments are: " ++ show ts

showTerm :: Term -> String
showTerm (Structure "[" ts) = "[" ++ showList ts ++ "]"
showTerm (Structure s []) = s
showTerm (Structure s ts) = s ++ "(" ++ (intercalate ", " . map showTerm $ ts) ++ ")"
showTerm (Assignable _ s) = s

showDefinition :: Definition -> String
showDefinition (Requisite t) = showTerm t
showDefinition (Unification t1 t2) = showTerm t1 ++ " = " ++ showTerm t2
showDefinition (Or d1 d2) = "(" ++ showDefinition d1 ++ "; " ++ showDefinition d2 ++ ")"
showDefinition (And d1 d2) = showDefinition d1 ++ ", " ++ showDefinition d2
showDefinition Fact = ""

showStatement :: Statement -> String
showStatement s = case s of
        Declaration t Fact -> showTerm t
        Declaration t d -> showTerm t ++ " :- " ++ showDefinition d
        Query d -> "?- " ++ showDefinition d
    ++ "."

-- substitution as a result of unification is a map of a set of equivalent variables to their possible Structure term
type Substitution = Map.Map (Set.Set String) (Maybe Term)

emptySubstitution :: Substitution
emptySubstitution = Map.empty

showSubstitution :: Substitution -> String
showSubstitution = (\x -> if null x then "true" else x)
                        . intercalate ",\n"
                        -- convert equivalent variables to chain of equals and possibly the last one equal to the resulting structure
                        . map (\(k, v) -> intercalate ", " . filter (not . null) $ chainEqual k:lastEqual (last k) v)
                        . varsToShow
    where
        chainEqual :: [String] -> String
        chainEqual k = intercalate ", " . map (\(x, y) -> x ++ " = " ++ y) $ zip k (tail k)

        lastEqual :: String -> (Maybe Term) -> [String]
        lastEqual _ Nothing = []
        lastEqual v (Just t) = [v ++ " = " ++ showTerm t]

        getTermVars :: Term -> Set.Set String
        getTermVars (Assignable _ s) = if isPrefixOf "$" s then Set.singleton s else Set.empty
        getTermVars (Structure _ args) = foldr Set.union Set.empty . map getTermVars $ args

        onlyExternalVars :: Substitution -> Set.Set String
        -- filter out wildcards and internally renamed variables and then filter out empty resulting sets or sets with only one unexpanded single variable.
        -- Put all the remaining variables together
        onlyExternalVars = foldr (\(k, _) a -> Set.union k a) Set.empty
                        . filter (\(k, v) -> not $ Set.null k || Set.size k == 1 && isNothing v)
                        . Map.foldrWithKey (\k v a -> (Set.filter (not . isPrefixOf "$") k, v):a) []

        necessaryVars :: Substitution -> Set.Set String -> Set.Set String -> Set.Set String
        -- take the onlyExternalVars, and check what internal variables they depend on.  Then for the newly found variables repeat the
        -- process until no new variables are found.
        necessaryVars subst vars result = if Set.null news then result else newsRecursive
            where
                terms = catMaybes . Map.foldrWithKey (\_ v a -> v:a) [] . Map.filterWithKey (\k _ -> not . Set.null $ Set.intersection k vars) $ subst
                deps = foldr Set.union Set.empty . map getTermVars $ terms
                news = Set.difference deps result
                newsRecursive = necessaryVars subst news (Set.union result news)

        varsToShow :: Substitution -> [([String], Maybe Term)]
        -- reduce the substitutions to a list, filtering out unnecessary variables and then
        -- filter out empty resulting sets or sets with only one unexpanded single variable.
        varsToShow subst = filter (\(k, v) -> not $ null k || length k == 1 && isNothing v)
                    . Map.foldrWithKey (\k v a -> (Set.toAscList . Set.filter (\s -> Set.member s necessaries) $ k, v):a) [] $ subst
                        where
                             exts = onlyExternalVars subst
                             necessaries = necessaryVars subst exts exts

lookupSubstitution :: String -> Substitution -> Maybe (Set.Set String, Maybe Term)
lookupSubstitution s = listToMaybe . Map.toList . Map.filterWithKey (\k _ -> Set.member s k)
