module Text.Pit.Unification (
        addSubstitution,
        mergeSubstitution,
        unifyAdd,
        unify,
        expandSubstitution,
        ) where

import qualified Data.Map as Map
import qualified Data.Set as Set
import Data.Maybe
import Text.Pit.Types

-- add one substitution element (e.g. X=[Y,Z], X=s(r)) to a substitution, either adding an element, merging with an existing set or merging some sets together
addSubstitution :: Set.Set String -> Maybe Term -> Substitution -> Maybe Substitution
addSubstitution vs t s = if Map.null common then return $ Map.insert vs t s
                        else if vs == commonKeys && listToMaybe commonValues == t && commonSubst == return s then return s
                        else commonSubst >>= addSubstitution commonKeys (listToMaybe commonValues)
        where
            -- get the substitutions that have a variable in common with this set
            (rest, common) = Map.partitionWithKey (\v2s _ -> Set.null $ Set.intersection vs v2s) s
            -- get a substitution that unifies all their values, as well as with the given value, starting from unrelated substitutions `rest`
            commonValues = catMaybes . (t:) . Map.elems $ common
            commonSubst = foldr (\(a1, a2) subst -> subst >>= unifyAdd a1 a2) (return rest) (zip commonValues (tail commonValues))
            -- get also a set that pours all the keys just unified together, as well as the given key
            commonKeys = foldr (\k a -> Set.union k a) Set.empty . (vs:) . Map.keys $ common

mergeSubstitution :: Substitution -> Substitution -> Maybe Substitution
mergeSubstitution s2 = Map.foldrWithKey (\k v s -> s >>= addSubstitution k v) (return s2)

unifyAdd :: Term -> Term -> Substitution -> Maybe Substitution
unifyAdd t1 t2 s = case (t1, t2) of
        -- if either of them is a wildcard, skip the unification altogether because no new substitution would come from that
        (Assignable "_" _, _) -> return s
        (_, Assignable "_" _) -> return s
        -- if both are assignable, they are simply equal and if they are both unified with a struct,
        -- those structs should be unified.
        (Assignable _ x, Assignable _ y) -> unifyVars x y
        -- if one is a variable and one is a Structure, the variable is simply equal to that Structure,
        -- or if it already unifies with a Structure, those Structures should be unified together.
        (Assignable _ x, _) -> unifyVarStruct x t2
        (_, Assignable _ y) -> unifyVarStruct y t1
        -- if two Structures, they should have the same name and arity, and their arguments must pairwise unify
        (Structure name1 args1, Structure name2 args2) -> unifyStructs name1 args1 name2 args2
    where
        unifyVars :: String -> String -> Maybe Substitution
        unifyVars x y
            | x == y = return s
            | otherwise = addSubstitution (Set.fromList [x, y]) Nothing s

        unifyVarStruct :: String -> Term -> Maybe Substitution
        unifyVarStruct x t = addSubstitution (Set.fromList [x]) (Just t) s

        unifyStructs :: String -> [Term] -> String -> [Term] -> Maybe Substitution
        unifyStructs name1 args1 name2 args2
            | name1 /= name2 || length args1 /= length args2 = Nothing
            | otherwise = foldr (\(a1, a2) subst -> subst >>= unifyAdd a1 a2) (return s) (zip args1 args2)

-- unify takes two terms and returns whether they are unifiable and if so what substitution to use
unify :: Term -> Term -> Maybe Substitution
unify t1 t2 = unifyAdd t1 t2 emptySubstitution

-- try to expand the terms each variable unifies with, substituting other variables to try to ground it
-- For example if I have:
--     [X, Y] -> z
--     W -> s(X)
--     T -> s(T)
-- this should make the target of W as s(z).  Although, the target of T cannot be
-- simplified further.  This requires that the map is a DAG, which is ensured because
-- the equivalent variables (cycles) are all put in one node.
expandSubstitution :: Substitution -> Substitution
expandSubstitution s = fst . Map.foldrWithKey expandOne (emptySubstitution, Set.empty) $ s
    where
        expandOne :: Set.Set String -> Maybe Term -> (Substitution, Set.Set String) -> (Substitution, Set.Set String)
        -- if a set of variables expand to struct, expand that struct itself
        expandOne vars (Just (Structure atom args)) (mapSoFar, visited)
                -- if already visited in the DFS, it's a loop or is already calculated, skip it
                | Set.null $ Set.difference vars visited = (mapSoFar, visited)
                -- | Map.member vars mapSoFar = (mapSoFar, Set.union visited vars)
                -- otherwise, visit its children, create a new map (building upon map built so far) and add the expanded current node to it
                | otherwise = (Map.union (Map.fromList [(vars, Just $ Structure atom margs)]) $ mchildren, vchildren)
            where
                (margs, mchildren, vchildren) = foldr (\a b -> expandOneDescend a b True) ([], mapSoFar, Set.union visited vars) args

                -- note: showSubstitution shows each row as `W = X, X = Y, Y = Z, Z = atom(args).`  Therefore,
                -- all variable names (first argument of Assignable) are changed to the biggest element in the
                -- set (so the last `Z = atom(args, containing, Z)` would look like a nice recursive definition).
                -- Both parameters of Assignable are changed, because neither of the previous values matter.  In fact,
                -- only the second parameter needs to change, but to keep the first element intact would just be
                -- misleading in debug
                expandOneDescend :: Term -> ([Term], Substitution, Set.Set String) -> Bool -> ([Term], Substitution, Set.Set String)
                expandOneDescend t@(Assignable _ y) (res, m, vstd) recurse
                        -- if the term is a variable and it is in the current set, just replace it by the last element
                        | Set.member y vars = ((replaceAssignable vars):res, m, vstd)
                        -- if it is not in the current set, find if it belongs to another set in the map of already expanded variables
                        | otherwise = case lookupSubstitution y m of
                                    -- if it expands to a structure, expand the variable to that structure, unless it's a recursive definition
                                    Just (vs, Just s2@(Structure _ args2)) -> if isRecursive args2
                                                                            then ((replaceAssignable vs):res, m, vstd)
                                                                            else (s2:res, m, vstd)
                                        where
                                            isRecursive :: [Term] -> Bool
                                            isRecursive = or . map isRecursiveOne
                                                where
                                                    isRecursiveOne :: Term -> Bool
                                                    isRecursiveOne (Structure _ as) = isRecursive as
                                                    isRecursiveOne (Assignable _ a) = Set.member a vs
                                    -- if it doesn't expand to anything, replace it by the last element of that set
                                    Just (vs, _) -> ((replaceAssignable vs):res, m, vstd)
                                    -- otherwise check to see if it belongs to another set in the original unexpanded substitution
                                    Nothing -> if recurse then case lookupSubstitution y s of
                                                -- if it expands to a structure, recursively calculate a new map and replace it afterwards
                                                Just (vs, s2@(Just (Structure _ _))) -> expandOneDescend t (res, m2, vstd2) False
                                                        where (m2, vstd2) = expandOne vs s2 (m, vstd)
                                                -- if it doesn't expand to anything, replace it by the last element of that set
                                                Just (vs, _) -> ((replaceAssignable vs):res, m, Set.union vstd vs)
                                                -- otherwise, it's a variable that is not substituted by anything
                                                Nothing -> noMoreExpand
                                          else
                                                noMoreExpand
                    where
                        noMoreExpand = (t:res, m, Set.insert y vstd)

                        replaceAssignable :: Set.Set String -> Term
                        replaceAssignable vs = Assignable maxVar maxVar
                                                where
                                                    maxVar = Set.findMax vs

                -- if the argument of the atom is another atom, the expansion is similar to the otherwise case of expandOne
                expandOneDescend (Structure atom2 args2) (res, m, vstd) recurse = ((Structure atom2 margs2):res, mchildren2, vchildren2)
                    where
                        (margs2, mchildren2, vchildren2) = foldr (\a b -> expandOneDescend a b recurse) ([], m, vstd) args2
        -- if a set of variables don't expand to anything, leave them be
        expandOne vars t (m, visited) = (Map.union m (Map.fromList [(vars, t)]), Set.union visited vars)
