module Text.Pit.Lexer (
        lex
        ) where

import Prelude hiding (lex, showChar)
import Data.Char (isSpace, isLower, isUpper, isAlphaNum, isPrint, ord)
import Data.List (isPrefixOf)
import Data.Maybe (isJust)
import Control.Monad.Writer
import Text.Pit.Types

-- Content is a string added with the information of what line number and which column we are currently in
type Content = (String, Int, Int)

lex :: String -> Writer [Message] [Token]
lex = sequence . lexAll . (\s -> (s, 1, 1))
    where
        showChar c
            | isPrint c = ['\'', c, '\'']
            | otherwise = '#':(show $ ord c)

        -- make a maybe value based on a Bool
        makeMaybe :: Bool -> a -> Maybe a
        makeMaybe p x = if p then return x else Nothing

        -- advance the content by one, taking care of line and column numbers
        advance :: Content -> Content
        advance (x:xs, l, c) = (xs, l', c')
                            where
                                (l', c') = if x == '\n' then (l + 1, 1) else (l, c + 1)
        advance ([], _, _) = error "internal error: advance shouldn't have been called with an empty string"

        -- advance the content by n
        advance' n content = iterate advance content !! n

        -- helper functions for matching tokens
        -- add a matched string to output of functions that just return the new content
        addString s content = return (s, content)
        -- match a single character
        matchExact :: Char -> Content -> Maybe Content
        matchExact y content@(x:_, _, _) = makeMaybe (x == y) $ advance content
        matchExact _ _ = error "internal error: matchExact should have been called with non-empty content"
        -- optionally match a single character
        optionalExact :: Char -> Content -> Maybe Content
        optionalExact y content@(x:_, _, _) = return $ if y == x then advance content else content
        optionalExact _ _ = error "internal error: optionalExact should have been called with non-empty content"
        -- match an exact string
        matchExactStr :: String -> Content -> Maybe Content
        matchExactStr ys content@(xs, _, _) = makeMaybe (isPrefixOf ys xs) $ advance' (length ys) content
        -- match while pattern holds for characters
        matchPattern :: (Char -> Bool) -> Content -> Maybe (String, Content)
        matchPattern p content@(xs, _, _) = makeMaybe (len > 0) (pfx, advance' len content)
                                    where
                                        pfx = takeWhile p xs
                                        len = length pfx
        -- match while pattern holds for string
        matchPatternStr :: (String -> Bool) -> Content -> Maybe (String, Content)
        matchPatternStr p content@(xs, _, _) = makeMaybe (len > 0) (pfx, advance' len content)
                                    where
                                        -- takeWhileStr is like takeWhile, but it takes while the predicate on the whole string succeeds
                                        takeWhileStr [] = []
                                        takeWhileStr str@(y:ys)
                                                    | p str = y:takeWhileStr ys
                                                    | otherwise = []
                                        pfx = takeWhileStr xs
                                        len = length pfx

        -- match the first function that succeeds
        matchAny :: [(Content -> Maybe a)] -> Content -> Maybe a
        matchAny fs content = foldr (\f acc -> let c = f content in if isJust c then c else acc) Nothing fs
        -- TODO: matchAny fs content = Data.Foldable.asum $ map ($ content) fs
        -- TODO: if successful, probably matchWhile and matchSequence can also be written in terms of <|> and folds
        -- match while match function succeeds
        matchWhile :: (Content -> Maybe (String, Content)) -> Content -> Maybe (String, Content)
        matchWhile f = f >=> return . matchWhile'
            where
                matchWhile' cur@(p, cnt) = case f cnt of
                                                Nothing -> cur
                                                Just (s, cnt2) -> matchWhile' $ (p ++ s, cnt2)
        -- TODO: matchWhile can be written with matchSequence given `repeat f`
        -- match a sequence of functions if at least one succeeds
        matchSequence :: [(Content -> Maybe (String, Content))] -> Content -> Maybe (String, Content)
        matchSequence (f1:fs1) = f1 >=> return . matchSequence' fs1
            where
                matchSequence' [] cur = cur
                matchSequence' (f:fs) cur@(p, cnt) = case f cnt of
                                                Nothing -> cur
                                                Just (s, cnt2) -> matchSequence' fs (p ++ s, cnt2)
        matchSequence _ = error "internal error: matchSequence should have been called with non-empty list of functions"
        -- match any whitespace
        matchSpace :: Content -> Maybe Content
        matchSpace = matchPattern isSpace >=> return . snd
        -- match one line comments
        matchOneLineComment :: Content -> Maybe Content
        matchOneLineComment = matchExact '%' >=> matchPattern (/= '\n') >=> optionalExact '\n' . snd
        -- match C style comments
        matchCStyleComment :: Content -> Maybe Content
        matchCStyleComment = matchExactStr "/*" >=> matchPatternStr (not . (isPrefixOf "*/")) >=> matchExactStr "*/" . snd
        -- helper function to match a letter to any of the given criteria
        orFunctions fs x = or $ map ($ x) fs
        -- match a string
        matchString :: Content -> Maybe (String, Content)
        matchString = matchExact '\'' >=> matchWhile (matchAny [matchPattern (/= '\''), addString "''" <=< matchExactStr "''"])
                                    >=> (\(s, c) -> matchExact '\'' c >>= addString s)
        -- match all character atoms
        matchAtomAllCharacter :: Content -> Maybe (String, Content)
        matchAtomAllCharacter = matchSequence [matchPattern isLower, matchPattern $ orFunctions [isAlphaNum, (== '_')]]
        -- match all graphic atoms
        matchAtomAllGraphic :: Content -> Maybe (String, Content)
        matchAtomAllGraphic = matchPattern isGraphic
            where
                isGraphic = flip elem $ "#&*+-./:<=>?@\\^~"

        -- add a token type to functions that just return a new content
        addType t c = Just (t, c)

        -- functions that match a token
        matchParenOpen = matchExact '(' >=> addType TParenOpen
        matchParenClose = matchExact ')' >=> addType TParenClose
        matchComma = matchExact ',' >=> addType TComma
        matchDot = matchExact '.' >=> addType TDot
        matchImpliedBy = matchExactStr ":-" >=> addType TImpliedBy
        matchQuery = matchExactStr "?-" >=> addType TQuery
        matchEqual = matchExact '=' >=> addType TEqual
        matchBracketOpen = matchExact '[' >=> addType TBracketOpen
        matchBracketClose = matchExact ']' >=> addType TBracketClose
        matchBar = matchExact '|' >=> addType TBar
        matchOr = matchExact ';' >=> addType TOr
        matchWhiteSpace = matchAny [matchSpace, matchOneLineComment, matchCStyleComment] >=> addType TWhiteSpace
        matchVariable = matchSequence [matchPattern $ orFunctions [isUpper, (== '_')], matchPattern $ orFunctions [isAlphaNum, (== '_')]]
                                    -- if suffix is empty and p is _, then don't match as variable
                                    >=> (\(s, c) -> makeMaybe (s /= "_") (TVariable s, c))
        matchWildCard = matchExact '_' >=> addType TWildCard
        matchAtom = matchAny [return . ((,) "''") <=< matchExactStr "''", matchString, matchAtomAllCharacter, matchAtomAllGraphic]
                                    >=> (\(s, c) -> return (TAtom s, c))

        lexOne :: Content -> (Writer [Message] Token, Content)
        lexOne cur@([], l, c) = (return (TEnd, l, c), cur)
        lexOne cur@(x:_, l, c) = let tokenMatchers = [matchParenOpen,
                                                      matchParenClose,
                                                      matchComma,
                                                      matchDot,
                                                      matchImpliedBy,
                                                      matchQuery,
                                                      matchEqual,
                                                      matchBracketOpen,
                                                      matchBracketClose,
                                                      matchBar,
                                                      matchOr,
                                                      matchWhiteSpace,
                                                      matchVariable,
                                                      matchWildCard,
                                                      matchAtom
                                                     ] in
                                case msum $ map ($ cur) tokenMatchers of
                                    -- ignore whitespace tokens
                                    Just (TWhiteSpace, cnt) -> lexOne cnt
                                    -- if nothing could be matched, generate an error and skip the character
                                    Nothing -> (tell [("Astray character " ++ showChar x, l, c)] >> token, cnt)
                                                where
                                                    (token, cnt) = lexOne $ advance cur
                                    -- otherwise, this is an interesting token
                                    Just (t, cnt) -> (return (t, l, c), cnt)

        lexAll :: Content -> [Writer [Message] Token]
        lexAll ([], _, _) = []
        lexAll content = token:lexAll rest
                    where
                        (token, rest) = lexOne content
